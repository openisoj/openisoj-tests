package org.bitbucket.openisoj;

import org.mockito.ArgumentMatcher;

public class FieldMatcher<T extends Iso8583> extends ArgumentMatcher<T> {
	private String value;
	private int field;

	public FieldMatcher(int field, String value) {
		this.field = field;
		this.value = value;
	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean matches(Object argument) {
		if (argument == null)
			return false;

		T msg = (T) argument;
		return value.equals(msg.get(field));
	}
}
