package org.bitbucket.openisoj;

import org.mockito.ArgumentMatcher;

public class MsgTypeMatcher<T extends Iso8583> extends ArgumentMatcher<T> {
	private int msgType;

	public MsgTypeMatcher(int msgType) {
		this.msgType = msgType;
	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean matches(Object argument) {
		if (argument == null)
			return false;

		T msg = (T) argument;
		return msgType == msg.getMsgType();
	}
}
