package org.bitbucket.openisoj;

import org.mockito.Mockito;

public class IsoMatchers {
	public static <T extends Iso8583> T msgWithField(Class<T> clazz, int field, String value) {
		return Mockito.argThat(new FieldMatcher<T>(field, value));
	}

	public static <T extends Iso8583> T msgWithMsgType(Class<T> clazz, int msgType) {
		return Mockito.argThat(new MsgTypeMatcher<T>(msgType));
	}
}
