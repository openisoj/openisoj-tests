package org.bitbucket.openisoj;

import static org.bitbucket.openisoj.IsoMatchers.*;
import static org.mockito.Mockito.*;

import org.bitbucket.openisoj.Iso8583.Bit;

public class FieldMatcherSamples {
	private TestGateway gateway;

	public void testThrowException() {
		when(gateway.send(msgWithField(Iso8583.class, Bit._011_SYS_TRACE_AUDIT_NUM, "000001"))).thenThrow(
				new RuntimeException("Stan 1"));
	}
}
