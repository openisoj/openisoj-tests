package org.bitbucket.openisoj;

public interface TestGateway {
	Iso8583 send(Iso8583 msg);
}
