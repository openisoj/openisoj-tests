# What is this

A unit test helper framework for [openisoj-core](http://bitbucket.org/openisoj). Still in alpha.

# Install with Maven

    <dependency>
        <groupId>org.bitbucket.openisoj</groupId>
        <artifactId>openisoj-tests</artifactId>
        <version>0.2.0</version>
        <scope>tests</scope>
    </dependency>

Check [search.maven.org](http://search.maven.org/#search%7Cga%7C1%7Ca%3A%22openisoj-tests%22) for the latest version.